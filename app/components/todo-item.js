import Component from '@ember/component';
import { alias } from '@ember/object/computed';

export default Component.extend({
    tagName: 'li',
    classNames: ['todo-item'],
    classNameBindings: ['isCompleted:completed'],

    isCompleted: alias('item.isCompleted'),

    actions: {
        deleteItem() {
            let item = this.get('item');

            return item.destroyRecord().then(() => {
                this.get('onItemDelete')(item);
            });
        }
    }
});
