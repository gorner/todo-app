export default [
    {id: 1, title: 'some todo', priority: 2},
    {id: 2, title: 'another todo', priority: 5},
    {id: 3, title: 'yet another todo', priority: 5}
]