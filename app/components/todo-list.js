import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  store: service(),

  classNames: ['todo-list'],
  tagName: 'ul',

  items: computed(function () {
    return [];
  }),

  actions: {
    itemDeleted() {
      // not needed at the moment but maybe in future?
    }
  }
});
