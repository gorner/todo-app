import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({

  newTodoTitle: '',
  newTodoPriority: '',
  
  // priorityCounts: {
  //   2: 1,
  //   5: 2
  // },

  priorityCounts: computed('model', function() {
    let model = this.get('model');

    let counts = {};

    model.forEach((item) => {
      let priority = item.priority;
      if(counts.hasOwnProperty(priority)) {
        counts[priority] = counts[priority] + 1;
      } else {
        counts[priority] = 1;
      }
    });

    return counts;
  }),

  priorityCountsJSON: computed('priorityCounts', function() {
    return this.get('priorityCounts').toJSON();
  }),

  actions: {

    createTodo() {
      let todo = this.get('store').createRecord('todo', {
        title: this.get('newTodoTitle'),
        priority: this.get('newTodoPriority')
      });
      
      return todo.save().then(() => {
        this.set('newTodoTitle', '');
        this.set('newTodoPriority', '');
      });
    }
  }
})