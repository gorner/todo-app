import DS from 'ember-data';
import { computed } from '@ember/object';
import { isPresent } from '@ember/utils';

export default DS.Model.extend({
    title: DS.attr('string'),
    priority: DS.attr('number'),
    completedAt: DS.attr('date', {defaultValue: null}),

    isCompleted: computed('completedAt', {
        get() {
            return isPresent(this.get('completedAt'));
        },
        set(key, val) {
            this.set('completedAt', val ? new Date() : null);
            this.save();
            return val;
        }
    })
});
