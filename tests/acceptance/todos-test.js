import { module, test } from 'qunit';
import { visit, find, fillIn, click/*, currentURL*/ } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';

module('Acceptance | todos', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('visiting index', async function(assert) {
    assert.expect(10);

    await visit('/');

    // Using class selectors for now
    // TODO: refactor to use data attributes, e.g. via ember-test-selectors
    assert.ok(find('.todo-list'), 'The Todo List is present');
    assert.notOk(find('.todo-item'), 'No todo items are present');

    await fillIn('.new-todo-title', 'Create a new todo');
    await fillIn('.new-todo-priority', '42');
    await click('.new-todo .submit');

    assert.ok(find('.todo-item'), 'One todo item should now be present');
    assert.equal(find('.todo-item .item-title').textContent.trim(), 'Create a new todo', 'The todo item should have the correct title');
    assert.equal(find('.todo-item .item-priority').textContent.trim(), '42', 'The todo item should have the correct priority');
    assert.equal(find('.new-todo-title').value, '', 'The todo form should be cleared');
    assert.notOk(find('.todo-item').classList.contains('completed'), 'The item should not be marked as completed');

    await click('.todo-item .complete');

    assert.ok(find('.todo-item').classList.contains('completed'), 'The item should be marked as completed');

    await click('.todo-item .complete');

    assert.notOk(find('.todo-item').classList.contains('completed'), 'The item should no longer be marked as completed');

    await click('.todo-item .delete');

    assert.notOk(find('.todo-item'), 'The item should be removed');
  });

  test('show report', async function(assert) {

    await visit('/');

    // report is displayed
    assert.ok(find('.todo-report'), 'The report is displayed');

    // report has # of rows corresponding to # of unique priority values
    assert.ok(find('.todo-report tbody tr').length, 2);

    // report displays counts of todos correctly
    // assert.ok(find('.todo-report tbody tr:eq(0) td.count').length, 2);
  });
});

