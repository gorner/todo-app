import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | index', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('priorityCounts is generated correctly', function(assert) {
    let controller = this.owner.lookup('controller:index');
    controller.set('model', [
      {priority: 2},
      {priority: 3},
      {priority: 2}
    ]);
    assert.deepEqual(controller.get('priorityCounts'), {
      2: 2,
      3: 1
    });
  });
});
